# -*- coding: utf-8 -*-

import threading
import copy

import TreeConfig
import Utils_Format as Format

from TreeParser import TreeParser
from TreeDB import _TreeDB
from Utils_errors import *
import DebugInfo
class DialogTree(object):
	def __init__(self, default_treeID=0, verbose=False):
		self._lock = threading.RLock()
		self._CurOwnerID = None
		self._CurUserID = None
		self._CurBotID = None
		self._CurTree = None
		self._verbose = verbose
		self._TreeParser = TreeParser()
		self._TreeDB = _TreeDB
		self._DEFAULTTree = self._TreeParser.readTreeFromDB(default_treeID)

	def _getHistoryTree(self, collection, ownerID, userID, botID):
		"""
		获取用户与机器当前所处对话树节点位置，
		并将该位置的对话树存到 self._tree
		若无则将默认树 self._DEFAULTTree 存于 self._tree
		:param collection:
		:param ownerID:
		:param userID:
		:param botID:
		:return:
		"""
		if self._CurOwnerID and self._CurOwnerID == ownerID\
				and self._CurUserID and self._CurUserID == userID \
				and self._CurBotID and self._CurBotID == botID:
			return
		tree_id = self._TreeDB.getHistory(collection, ownerID, userID, botID)

		if tree_id:
			self._CurTree = self._TreeParser.readTreeFromDB(tree_id)
		else:
			self._CurTree = copy.deepcopy(self._DEFAULTTree)
		self._CurOwnerID = ownerID
		self._CurUserID = userID
		self._CurBotID = botID

	def _saveHistoryTree(self, collection, ownerID, userID, botID):
		"""
		存储用户与机器之间对话所对应的节点位置
		如果已到叶节点。则回到默认树 self._DEFAULTTree 的初始结点。

		:param collection: 数据库表名 (str)
		:param ownerID: 所有人ID (str)
		:param userID: 用户ID (str)
		:param botID: 机器ID (str)
		:return:
		"""
		try:
			if self._CurTree._is_leaf:
				self._CurTree = copy.deepcopy(self._DEFAULTTree)
			treeID = self._CurTree._node_id
			self._TreeDB.saveHistory(collection, ownerID, userID, botID, treeID)
		except:
			pass

	def _getResponse(self, result):
		sentence, answers, node_id = result
		if not sentence or not answer:
			return []
		txt = sentence+"#@"
		for ele in answers:
			txt += "#@" + ele
		response = {
			"txt": txt,
			"path": "Node ID:" + str(node_id)
		}
		return [response]

	def getCurSen(
			self,
			ownerID=TreeConfig.DEFAULT_ID,
			userID=TreeConfig.DEFAULT_ID,
			botID=TreeConfig.DEFAULT_ID,
			collection=TreeConfig.historyColl
	):
		"""
		获取 当前对话节点的机器人对话以及用户选项
		用于：
			在对话树过程中用户回答了非用户选项 而 跳转到其他模块获取回答时，回到对话树时提供给用户的对话以及选项
			以及 在对话树模块初始状态下提供给用户的对话以及选项

		:param ownerID: 所有人ID (str)
		:param userID: 用户ID (str)
		:param botID: 机器ID （str)
		:param collection: 数据库表名 (str)
		:return: 三个元素：sentence， answer， user_inf ，
				 依次代表当前对话 机器的话(str)，用户选项(list of str)，用户信息(dict)
		"""
		self._lock.acquire()
		try:
			self._getHistoryTree(collection, ownerID, userID, botID)
			self._saveHistoryTree(collection, ownerID, userID, botID)
			answer = [ele for ele in self._CurTree._children]
			return self._CurTree._sentence, answer
		except PyMongoTimeOutError as e:
			DebugInfo.context += ("Dialog Tree:\n\tMongoDB Error: {0}\n".format(e))
			print("Dialog Tree:\n\tMongoDB Error: {0}".format(e))
			return None, None
		finally:
			self._lock.release()

	def _match(
			self, query,
			ownerID=TreeConfig.DEFAULT_ID,
			userID=TreeConfig.DEFAULT_ID,
			botID=TreeConfig.DEFAULT_ID,
			collection=TreeConfig.historyColl,
			user_inf ={}
	):
		"""
		根据用户输入 query 查询对话树

		:param query: 用户输入的句子 (str)
		:param ownerID: 所有人ID (str)
		:param userID: 用户ID (str)
		:param botID: 机器ID (srt)
		:param collection: 数据库表名 (str)
		:param user_inf: 用户信息表 （dict）
		:return: 三个元素：sentence， answer， user_inf ，
				依次代表当前对话 机器的话(str)，用户选项(list of str)，用户信息(dict)
		"""
		if isinstance(query, str):
			query = query.decode("utf-8")
		self._lock.acquire()
		try:
			self._getHistoryTree(collection, ownerID, userID, botID)
			user_inf["ownerID"] = ownerID
			user_inf["userID"] = userID
			user_inf["botID"] = botID
			_old_tree = self._CurTree
			while True:
				self._CurTree, user_inf = self._CurTree(query, user_inf)
				node_id = self._CurTree._node_id
				if not self._CurTree:
					self._CurTree = _old_tree
					return None, None, node_id
				elif self._CurTree.isSelect():
					break
			answer = [ele for ele in self._CurTree._children]
			retV = self._CurTree._sentence, answer, self._CurTree._node_id
			self._saveHistoryTree(collection, ownerID, userID, botID)
			return retV
		finally:
			self._lock.release()

	def match(self, ownerid, userid, botid, timemark, moduletype, query):
		try:
			if moduletype != TreeConfig.moduletype:
				# retV = format.getFormat(ownerid, userid, botid, timemark)
				retV = Format.getFormat(ownerid, userid, botid, timemark, TreeConfig.moduletype, query,
										[], TreeConfig.resp_type_error)
			else:
				result = self._match(query, ownerid, userid, botid)
				response_list = self._getResponse(result)
				retV = Format.getFormat(ownerid, userid, botid, timemark, TreeConfig.moduletype, query,
										response_list, TreeConfig.resp_success)
			return retV
		except PyMongoTimeOutError as e:
			DebugInfo.context += ("Dialog Tree:\n\tMongoDB Error: {0}\n".format(e))
			print("Dialog Tree:\n\tMongoDB Error: {0}".format(e))
			retV = Format.getFormat(ownerid, userid, botid, timemark, TreeConfig.moduletype, query,
									[], TreeConfig.resp_time_out)
			return retV
		except Exception as e:
			DebugInfo.context += ("Dialog Tree:\n\t Unknown Error: {0}\n".format(e))
			print("Dialog Tree:\n\t Unknown Error: {0}".format(e))
			retV = Format.getFormat(ownerid, userid, botid, timemark, TreeConfig.moduletype, query,
									[], TreeConfig.resp_unknow_error, str(e))
			return retV


if __name__ == "__main__":
	talk = DialogTree(default_treeID=0)
	user_inf = {"age": 19}
	sentence, selections = talk.getCurSen("owner", "wz1", "alice")
	DebugInfo.context += sentence +"\n"
	print(sentence)
	if selections:
		for answer in selections:
			DebugInfo.context += answer +"\n"
			print(answer),

	while True:
		query = raw_input("\n")
		if query == "/":
			sentence, selections = talk.getCurSen("owner", "wz1", "alice")
			DebugInfo.context += sentence +"\n"
			print(sentence)
			if selections:
				for answer in selections:
					DebugInfo.context += answer + "\n"
					print(answer),
			continue
		# sentence, selections, tmp = talk.match(query, "owner", "wz1", "alice")
		# response = talk.match("owner", "wz", "alice", "123456", TreeConfig.moduletype, query)
		response = talk.match("owner", "wz", "alice", "123456", "123", query)
		DebugInfo.context += response + "\n"
		print(response)


