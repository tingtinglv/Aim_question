# -*- coding:utf-8 -*-

import TreeConfig
from TreeConfig import parser_condition
from TreeDB import _TreeDB




class Node(object):
	"""
	Node 类，为对话树的节点的类型。
	属性:
		_sentence		: 存储当前节点输出的话 ( 条件判断节点 该属性无效)
		_conditions		: 存储条件判断节点的子节点条件 ( 对话选择节点 该属性无效)
		_children		: 存储其子节点
		_tree_id		: 存储当前节点id
		_is_leaf		: 表示当前结点是否是叶节点
		_is_root		: 表示当前节点是否是根节点
		_type			: 节点类型，现包括 条件节点 与 选择节点 两类， 对应不同的处理函数。
		_operation		: 节点操作 (未实际实现。待设计)

		_tmp_user_inf	: 临时存储用户信息，用以条件判断
	"""
	def __init__(self, node_id=None, sentence="", _type=TreeConfig.default_type, is_leaf=False, is_root=False, operation=None):
		self._sentence = sentence
		self._conditions = []
		self._children = {}
		self._node_id = node_id
		self._is_leaf = is_leaf
		self._is_root = is_root
		self._type = _type
		self._operation = None

		self._tmp_user_inf = None

	def isSelect(self):
		"""
		返回当前节点是否是 用户选择节点
		目前只有用户选择节点存储 robot 对话。
		:return:
		"""
		return self._type == TreeConfig.selection_type

	def addChild(self, key, sub_node=None):
		"""
		添加子节点
		:param key: 对话选择节点中 key 为用户的对话选项 ；条件判断节点中 key 为子树对应的条件选项索引
		:param sub_node:子节点，类型为Tree
		"""
		self._children[key] = sub_node

	def addCondition(self, condition, sub_node=None):
		idx = len(self._conditions)
		self._conditions.append(condition)
		self.addChild(idx, sub_node)

	def _match_all(self, flags, condition):
		fun = condition[0]
		user_elem = condition[1]
		default_elem = condition[2]
		print ("function : {0} \tuser elem : {1} \tdefault_elem : {2}".format(fun, user_elem, default_elem))
		return flags and parser_condition[fun](self._tmp_user_inf, user_elem, default_elem)

	def _process(self, user_answer, user_inf):
		"""
		未定 可能之后调用外部接口
		:param user_answer:
		:param user_inf:
		:return:
		"""
		return user_inf

	def _select(self, user_answer, user_inf):
		if user_answer in self._children:
			user_inf = self._process(user_answer, user_inf)
			return self._children[user_answer], user_inf
		else:
			return None, user_inf

	def _judge(self, user_answer, user_inf):
		self._tmp_user_inf = user_inf
		for idx, condition_list in enumerate(self._conditions):
			if reduce(self._match_all, condition_list, True):
				user_inf = self._process(user_answer, user_inf)
				return self._children[idx], user_inf
		return None, user_inf

	def _getNext(self, user_answer, user_inf):
		if isinstance(user_answer, str):
			user_answer = user_answer.decode("utf-8")

		if self._type == TreeConfig.selection_type:
			return self._select(user_answer, user_inf)
		elif self._type == TreeConfig.condition_type:
			return self._judge(user_answer, user_inf)

	def __call__(self, user_answer, user_inf={}):
		return self._getNext(user_answer, user_inf)
