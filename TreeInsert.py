#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2017/11/28 10:37
# @Author  : tignting.lv
# @Site    : 
# @File    : TreeInsert.py
# @Software: PyCharm
#@Contact  : sunfiyes@163.com
from MongoDB import _mongoDB
from TreeConfig import *
# date = {"NID":0,"SentenceID":1,"A&N":{0:1,1:2},"Root":"TRUE","Leaf":"FALSE","Type":"selection","Operation":""}
# _mongoDB.insert(tree_collection, date)
# date = {"NID":1,"SentenceID":0,"A&N":{2:3,1:7},"Root":"FALSE","Leaf":"FALSE","Type":"condition","Operation":""}
# _mongoDB.insert(tree_collection, date)
# date = {"NID":2,"SentenceID":3,"A&N":{2:4,3:6},"Root":"FALSE","Leaf":"FALSE","Type":"selection","Operation":""}
# _mongoDB.insert(tree_collection, date)
# date = {"NID":3,"SentenceID":2,"A&N":{2:5,3:6},"Root":"FALSE","Leaf":"FALSE","Type":"selection","Operation":""}
# _mongoDB.insert(tree_collection, date)
date = {"NID":4,"SentenceID":5,"A&N":{},"Root":"FALSE","Leaf":"TRUE","Type":"selection","Operation":""}
_mongoDB.insert(tree_collection, date)
date = {"NID":5,"SentenceID":4,"A&N":{},"Root":"FALSE","Leaf":"TRUE","Type":"selection","Operation":""}
_mongoDB.insert(tree_collection, date)
date = {"NID":6,"SentenceID":6,"A&N":{},"Root":"FALSE","Leaf":"TRUE","Type":"selection","Operation":""}
_mongoDB.insert(tree_collection, date)
date = {"NID":7,"SentenceID":7,"A&N":{},"Root":"FALSE","Leaf":"TRUE","Type":"selection","Operation":""}
_mongoDB.insert(tree_collection, date)


# date = {"ConditionID":0,"Content":[["TRUE","TRUE","TRUE"]]}
# _mongoDB.insert(condition_collection,date)
# date = {"ConditionID":1,"Content":[["IS_LE", "age", 12]]}
# _mongoDB.insert(condition_collection,date)
# date = {"ConditionID":2,"Content":[["IS_GT", "age", 12]]}
# _mongoDB.insert(condition_collection,date)
#
#
# date = {"AnswerID":0,"Content":"男"}
# _mongoDB.insert(selection_collection, date)
# date = {"AnswerID":1,"Content":"女"}
# _mongoDB.insert(selection_collection, date)
# date = {"AnswerID":2,"Content":"好呀"}
# _mongoDB.insert(selection_collection, date)
# date = {"AnswerID":3,"Content":"不感兴趣"}
# _mongoDB.insert(selection_collection, date)
#
# date = {"SentenceID":0,"Content":""}
# _mongoDB.insert(sentence_collection,date)
# date = {"SentenceID":1,"Content":"您的性别"}
# _mongoDB.insert(sentence_collection,date)
# date = {"SentenceID":2,"Content":"给你介绍一个游戏"}
# _mongoDB.insert(sentence_collection,date)
# date = {"SentenceID":3,"Content":"给你介绍一个美容方法"}
# _mongoDB.insert(sentence_collection,date)
# date = {"SentenceID":4,"Content":"MineCraft是一款XXXX"}
# _mongoDB.insert(sentence_collection,date)
# date = {"SentenceID":5,"Content":"早睡早起，多喝热水"}
# _mongoDB.insert(sentence_collection,date)
# date = {"SentenceID":6,"Content":"那再见"}
# _mongoDB.insert(sentence_collection,date)
# date = {"SentenceID":7,"Content":"好好学习，天天向上"}
# _mongoDB.insert(sentence_collection,date)


