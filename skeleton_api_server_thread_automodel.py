#!/usr/bin/env python
#*--encoding=utf-8--*
##################################################
#简单的aiml服务端框架
#时间:2017-07-17
#版本:beta
#作者:shx
#基于BaseHTTPServer的http server实现，包括get，post方法，get参数接收，post参数接收。
#################################################
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from SocketServer import ThreadingMixIn
from urllib import unquote
import os,time,socket,threading
import io,shutil,urllib,getopt,string
import json,re
from datetime import datetime
import RBCS as aiml
import DebugInfo
class MyRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.process(2)
    def do_POST(self):
        self.process(1)
    def process(self,type):
        content =""
        print self.path
        if type==1:#post方法，接收post参数
            datas = self.rfile.read(int(self.headers['content-length']))
            datas = urllib.unquote(datas).decode("utf-8", 'ignore')#指定编码方式
            datas = transDicts(datas)#将参数转换为字典
            if datas.has_key('data'):
                content = "data:"+datas['data']+"\r\n"
        if '?' in self.path:
            query = urllib.splitquery(self.path)
            DebugInfo.context += "query: %s thread=%s" %(" ".join(query),str(threading.current_thread())) + "\n"
            print "query: %s thread=%s" %(" ".join(query),str(threading.current_thread()))
            action = query[0]
            if query[1]:#接收get参数
                queryParams = {}
                for qp in query[1].split('&'):
                    kv = qp.split('=')
                    queryParams[kv[0]] = urllib.unquote(kv[1])
                    queryParams[kv[0]] = unquote(queryParams[kv[0]]).decode("utf-8", 'ignore')
                    #content+= kv[0]+':'+queryParams[kv[0]]+"\r\n"
                content = jsonPack(queryParams)
            #指定返回编码
            enc="UTF-8"
            content = content.encode(enc)
         #   print content
            f = io.BytesIO()
            f.write(content)
            f.seek(0)
            try:
                self.send_response(200)
                self.send_header("Content-type", "text/html; charset=%s" % enc)
                self.send_header("Content-Length", str(len(content.decode("utf-8", 'ignore'))))
                self.end_headers()
                shutil.copyfileobj(f,self.wfile)
            except socket.error,e:
                print "socket.error : Connection broke. Aborting" + str(e)
                self.wfile._sock.close() # close socket
                self.wfile._sock=None
                return False
            DebugInfo.context += "success prod query :%s" %(" ".join(query)) + "\n"
            print "success prod query :%s" %(" ".join(query))
            return True


class ThreadingHTTPServer(ThreadingMixIn,HTTPServer):
    pass

def transDicts(params):
    dicts={}
    if len(params)==0:
        return
    params = params.split('&')
    for param in params:
        dicts[param.split('=')[0]]=param.split('=')[1]
    return dicts

def function(source):
    # 将输入输入函数并输出封装字典
    global alice
    DebugInfo.context += str(datetime.now()) + "\n" +  'query: ', source + "\n"
    print str(datetime.now())
    print 'query: ', source
    outputDic = {}
    answer = alice.respond(source)
    outputDic["cmnt"] = answer
    return outputDic


def jsonPack(dic):
    #json 打包工具.添加几个key.然后打包. 
    query=dic["query"].strip("+")
    result_dic=function(query)
    result = json.dumps(result_dic)
    return result


if __name__=='__main__':
    try:
        os.chdir('./alice')
        alice = aiml.Kernel()
        alice.learn("startup.xml")
        alice.respond('LOAD ALICE')
        server = ThreadingHTTPServer(('127.0.0.1',9350), MyRequestHandler)
        DebugInfo.context +=  'started aiml multithreading httpserver... on port 9350\n'
        print 'started aiml multithreading httpserver... on port 9350'
        server.serve_forever()

    except KeyboardInterrupt:
        server.socket.close()
    pass

