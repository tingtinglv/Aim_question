# -*- coding:utf-8 -*-
from MongoDB import _mongoDB

def default_true(user_inf, user_elem=True, default_elem=True):
	return True


def is_empty(user_inf, user_elem, default_elem=True):
	print("in is_empty {0},{1}".format(user_elem,user_inf))
	if user_elem in user_inf:
		# print("not empty")
		return False
	else:
		# print("empty")
		return True


def is_equal(user_inf, user_elem, default_elem):
	print(user_elem)
	if user_elem in user_inf and user_inf[user_elem] == default_elem:
		# print("equal")
		return True
	else:
		# print("not equal")
		return False


def is_gt(user_inf, user_elem, default_elem):
	int_idx = default_elem.find(".")
	default_elem = int(default_elem[:int_idx])
	if user_elem in user_inf and user_inf[user_elem] > default_elem:
		# print("great than")
		return True
	else:
		# print("not great than")
		return False


def is_lt(user_inf, user_elem, default_elem):
	int_idx = default_elem.find(".")
	default_elem = int(default_elem[:int_idx])
	if user_elem in user_inf and user_inf[user_elem] < default_elem:
		return True
	else:
		return False
