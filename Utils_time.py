# -*- coding:utf-8 -*-
import time


def getCurrentTime():
	return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))


def isVaild(datetime):
	try:
		date = time.strptime(datetime, "%Y-%m-%d %H:%M:%S")
		date = int(time.mktime(date))
		cur = int(time.time())
		if cur - date < 3600:
			return True
		else:
			return False
	except:
		return False
